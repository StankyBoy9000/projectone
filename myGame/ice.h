#ifndef ICE_H_INCLUDED
#define ICE_H_INCLUDED

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "spriteObject.h"
#include <iostream>

using namespace std;

class Ice : public SpriteObject{
public:
    Ice(SDL_Renderer*,int, int,int,int,int,int);
    void draw(SDL_Renderer*);
};

#endif // ICE_H_INCLUDED
