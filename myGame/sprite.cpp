#include "sprite.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include <string>
#include <math.h>
#include <SDL2/SDL_ttf.h>
#include <sstream>
#include <Windows.h>
#include <MMSystem.h>
using namespace std;

/** Kreiranje kruga koji sluzi za proveravanje kolizije */
struct Circle {
    int x;
    int y;
    double r;
};
/** Provera kolizije na osnovu objekta sa kojim se sudara i zeljenog kretanja*/
bool Sprite::isCollision(SpriteObject* object,int mX, int mY){
        /** Kratka prepravka koordinata Sprite-a koje dolze u obzir za koliziju, poenta je da se proverava samo njegova donja polovina, deluje realnije*/
        int x = spriteRect->x + spriteRect->w/2 + mX;
        int y = spriteRect->y + spriteRect->h/2+spriteRect->h/4 + mY;

        int xo = object->spriteRecta->x + object->spriteRecta->w/2;
        int yo = object->spriteRecta->y + object->spriteRecta->h/2;

        Circle hero;
        Circle wall;

        hero.x = x;
        hero.y = y;
        hero.r = spriteRect->w/2.3;

        wall.x = xo;
        wall.y = yo;
        wall.r = object->spriteRecta->w/2.3;

        double d = sqrt( (wall.x - hero.x)*(wall.x - hero.x) + (wall.y - hero.y)*(wall.y - hero.y) );

        if(d < hero.r + wall.r){
            return true;
        }
    return false;
    }
/** Ista metoda samo ova uzima Vatrene loptice u obzir samo, smer kretanja Sprite-a u ovom slucaju je nebitan */
bool Sprite::isCollision(Fireball* object){

        int x = spriteRect->x + spriteRect->w/2;
        int y = spriteRect->y + spriteRect->h/2+spriteRect->h/4;

        int xo = object->fireballRect->x + object->fireballRect->w/2;
        int yo = object->fireballRect->y + object->fireballRect->h/2;

        Circle hero;
        Circle wall;

        hero.x = x;
        hero.y = y;
        hero.r = spriteRect->w/2.3;

        wall.x = xo;
        wall.y = yo;
        wall.r = object->fireballRect->w/2.3;

        double d = sqrt( (wall.x - hero.x)*(wall.x - hero.x) + (wall.y - hero.y)*(wall.y - hero.y) );

        if(d < hero.r + wall.r){
            object->active = false;
            return true;
        }
    return false;
    }
/** Provera da li Sprite napusta opseg mape, koriste se fiksirane koordinate, u slucaju da se menja velicina mape mora se i ova metoda menjati */
bool Sprite::outOfBounds(int dx, int dy){
    if(spriteRect->x +dx < 5   ){
        return true;
    }
    else if(spriteRect->x +dx > 577){
        return true;
    }
    else if(spriteRect->y + dy< -20){
        return true;
    }
    else if(spriteRect->y + dy> 257){
        return true;
    }
    return false;
}

/** Metoda za kretanje Sprite-a, u isto vreme i odredjuje koja se animacija treba crtati na osnovu lastDirection-a */
void Sprite::move(int dx, int dy) {
    /** U slucaju da Sprite kliza ili je van opsega mape odmah se izlazi iz metode. */
        if(isSkating || outOfBounds(dx,dy)){
            return;
        }
    /** U slucaju da ima kolizije vraca ga 1 px nazad na osnovu pravca kretanja, !!ovo nije najbolje resenje!! zato sto postoje situacije gde ce zbog toga Sprite prolaziti kroz objekte */
        if(hasCollision){
            if(lastDirection == 4){
                //spriteRect->x -= 1;
                frameRect->y = 192;
                currentFrame = 0;
            }
            else if (lastDirection == 1) {
                //spriteRect->x += 1;
                frameRect->y = 64;
                currentFrame = 0;
            }
            else if(lastDirection == 3) {
                //spriteRect->y -= 1;
                frameRect->y = 128;
                currentFrame = 0;
            }
            else if(lastDirection == 2) {
                //spriteRect->y += 1;
                frameRect->y = 0;
                currentFrame = 0;
            }
        }
        /** U slucaju da nema kolizije krece se u pravcu strelica koje su pritisnute */
        else if(!hasCollision){
            if(state != 0) {
                if(state & 1){
                    spriteRect->x += 1;
                    frameRect->y = 192;
                }
                if (state & 2) {
                    spriteRect->x -= 1;
                    frameRect->y = 64;
                }
                if(state & 4) {
                    spriteRect->y -= 1;
                    frameRect->y = 0;
                }
                 if(state & 8) {
                    spriteRect->y += 1;
                    frameRect->y = 128;
                }
        }
    }
}
/** Metoda za klizanje Sprite-a */
void Sprite::slide() {
    /** U slucaju da ne kliza, odmah se izlazi iz metode. */
    if(!isSkating){
        return;
    }
    /** Na osnovu poslednjeg pravca kretanja Sprite kliza sve dok ne izadje iz opsega objekta Ice */
    if(lastDirection == 4){
        frameRect->y = 192;
        spriteRect->x += 2;
    }
    else if (lastDirection == 1) {
        spriteRect->x -= 2;
        frameRect->y = 64;
    }
    else if(lastDirection == 2) {
        spriteRect->y -= 2;
        frameRect->y = 128;
    }
    else if(lastDirection == 3) {
        spriteRect->y += 2;
        frameRect->y = 0;
    }
}
void Sprite::drawDeath(SDL_Renderer* renderer, TTF_Font* font){
    if(!isAlive){
        SDL_Color white = {0, 255, 0};
        stringstream ss;
        ss << "Dead";
        SDL_Surface* sm = TTF_RenderText_Solid(font, ss.str().c_str(), white);
        SDL_Texture* poruka = SDL_CreateTextureFromSurface(renderer, sm);
        SDL_Rect poruka_box;
        poruka_box.x = 320;
        poruka_box.y = 700;
        poruka_box.w = 100;
        poruka_box.h = 100;
        SDL_RenderCopy(renderer, poruka, NULL, &poruka_box);
        return;
    }
}
void Sprite::deathSound(int counter){
    if(counter < 1){
        PlaySound(TEXT("roblox.wav"),NULL, SND_SYNC);
    }
}
/** Crtanje Sprite-a */
void Sprite::draw(SDL_Renderer *renderer) {
    if(!isAlive){
        return;
    }
    /** U slucaju da je ziv Sprite, crta se na osnovu pravca kretanja */
    if(!(state & 1 || state & 2 || state & 4 || state & 8)){
        if(lastDirection == 1){
            frameRect->y = 64;
            currentFrame = 0;
        }
        else if(lastDirection == 4){
            frameRect->y = 192;
            currentFrame = 0;
        }
        else if(lastDirection == 2){
            frameRect->y = 0;
            currentFrame = 0;
        }
        else if(lastDirection == 3){
            frameRect->y = 128;
            currentFrame = 0;
        }
    }

    frameRect->x = currentFrame*frameWidth;
    SDL_RenderCopy(renderer, spriteTexture, frameRect, spriteRect);
    if(state & 1 || state & 2 || state & 4 || state & 8){
        totalFrames++;
            if(totalFrames%4 == 0) {
                if(!isSkating){
                    currentFrame++;
                }
            if(currentFrame >= frameCount) {
                currentFrame = 0;
            }
        totalFrames = 0;
        }
    }
}

/** Konstruktor Sprite-a*/
Sprite::Sprite(SDL_Renderer *renderer){
        frameWidth = 64;
        frameHeight = 64;
        currentFrame = 0;
        totalFrames = 0;
        state = 0;

        SDL_Surface *surface = IMG_Load("resources/creatures/human1_walk.png");
        frameCount = surface->w/frameWidth;
        spriteTexture = SDL_CreateTextureFromSurface(renderer, surface);
        SDL_FreeSurface(surface);

        frameRect = new SDL_Rect;
        frameRect->y = 0;
        frameRect->h = frameHeight;
        frameRect->w = frameWidth;

        spriteRect = new SDL_Rect;
        spriteRect->x = 200;
        spriteRect->y = 200;
        spriteRect->w = frameWidth;
        spriteRect->h = frameHeight;
    }
