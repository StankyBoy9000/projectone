#include "spriteObject.h"
/** Crtanje objekata */
void SpriteObject::draw(SDL_Renderer *renderer) {
        SDL_RenderCopy(renderer, spriteTexture, frameRect, spriteRecta);
        totalFrames++;
        if(totalFrames%4 == 0) {
            currentFrame++;
            if(currentFrame >= frameCount) {
                currentFrame = 0;
            }
            totalFrames = 0;
        }
    }
