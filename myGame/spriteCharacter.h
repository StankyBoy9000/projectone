#ifndef SPRITE_H_INCLUDED
#define SPRITE_H_INCLUDED
#include <SDL2/SDL.h>
#include "drawable.h"
#include "sprite.h"

class SpriteObject : public Drawable{
private:
    SDL_Rect *spriteRect;
    SDL_Texture *spriteTexture;
    int frameWidth;
    int frameHeight;
    int frameCount;
    int currentFrame;
    int totalFrames;
    SDL_Rect *frameRect;
public:
    char state;
    SpriteCharacter(SDL_Renderer *renderer) {
        frameWidth = 64;
        frameHeight = 64;
        currentFrame = 0;
        totalFrames = 0;
        state = 0;

        SDL_Surface *surface = IMG_Load("resources/creatures/human1_walk2.png");
        frameCount = surface->w/frameWidth;
        spriteTexture = SDL_CreateTextureFromSurface(renderer, surface);
        SDL_FreeSurface(surface);

        frameRect = new SDL_Rect;
        frameRect->y = 0;
        frameRect->h = frameHeight;
        frameRect->w = frameWidth;

        spriteRect = new SDL_Rect;
        spriteRect->x = 0;
        spriteRect->y = 0;
        spriteRect->w = frameWidth;
        spriteRect->h = frameHeight;
    }

    void draw(SDL_Renderer *renderer) {
        frameRect->x = currentFrame*frameWidth;
        SDL_RenderCopy(renderer, spriteTexture, frameRect, spriteRect);
        totalFrames++;
        if(totalFrames%4 == 0) {
            currentFrame++;
            if(currentFrame >= frameCount) {
                currentFrame = 0;
            }
            totalFrames = 0;
        }
    }

    void move(int dx, int dy) {

        if(state != 0) {
            if(state & 1){
                spriteRect->x += dx;
            }
            if (state & 2) {
                spriteRect->x -= dx;
            }
            if(state & 4) {
                spriteRect->y -= dy;
            }
             if(state & 8) {
                spriteRect->y += dy;
            }
        }
    }
};
#endif // SPRITE_H_INCLUDED
