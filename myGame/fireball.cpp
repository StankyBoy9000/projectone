#include "fireball.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include <string>
#include <math.h>

using namespace std;

struct Tacka{
    int x;
    int y;
};
/** Funkcija koja proverava da li su vatrene loptice napustile opseg mape. */
bool Fireball::outOfBounds(){
    if(fireballRect->x< 5   ){
        return true;
    }
    else if(fireballRect->x> 577){
        return true;
    }
    else if(fireballRect->y< -20){
        return true;
    }
    else if(fireballRect->y> 257){
        return true;
    }
    return false;
}
/** Pokretanje vatrenih loptica, u slucaju da su napustile opseg vidljivosti nestaju. Tj. postaju neaktivne. */
void Fireball::move() {
    if(outOfBounds()){
        active = false;
        return;
    }
    /** Kratka prepravka koordinata na koje treba da cilja loptica, da ne bi gadjala gornji levi ugao Sprite-a pomereno je na odgovarajuce koordinate. */
    int xPrim = spriteX + 16 - fireballX;
    int yPrim = spriteY + 32 - fireballY;

    int speed = 5; /** Brzina kretanja loptice */

    /** ----------------Racunanje pravca kretanja loptica--------*/
    double alpha = atan2(yPrim,xPrim);

    double newX = cos(alpha);
    double newY = sin(alpha);

    fireballRect->x += newX * speed;
    fireballRect->y += newY * speed;
    /** ----------------------------------------------------------*/

}
 /** Crtanje vatrenih loptica */
void Fireball::draw(SDL_Renderer *renderer) {
    frameRect->x = 180; /** X koordinata sa slike koja se preslikava kasnije na "platno" */
    frameRect->y = 50;  /** Y koordinata sa slike koja se preslikava kasnije na "platno" */

    SDL_RenderCopy(renderer, fireballTexture, frameRect, fireballRect);
}

/** Kontsruktor vatrene loptice */
Fireball::Fireball(SDL_Renderer *renderer,int ex,int ey,int ox,int oy){
    spriteX = ox; /** X koordinata Sprite-a */
    spriteY = oy; /** Y koordinata Sprite-a */
    fireballX = ex; /** X koordinata lopte */
    fireballY = ey; /** Y koordinata lopte */
    frameWidth = 25; /** Sirina lopte na slici */
    frameHeight = 25; /** Visina lopte na slici */

    SDL_Surface *surface = IMG_Load("resources/creatures/explosion.png"); /** Adresa odakle se uzimaju koordinate vatrene lopte */
    fireballTexture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);

    frameRect = new SDL_Rect;
    frameRect->h = frameHeight;
    frameRect->w = frameWidth;

    fireballRect = new SDL_Rect;
    fireballRect->x = ex;
    fireballRect->y = ey;
    fireballRect->w = frameWidth;
    fireballRect->h = frameHeight;
}
