#ifndef SPRITE_H_INCLUDED
#define SPRITE_H_INCLUDED
#include <SDL2/SDL.h>
#include "spriteObject.h"
#include "drawable.h"
#include "fireball.h"
#include <SDL2/SDL_ttf.h>

class Sprite : public Drawable{
private:

    SDL_Texture *spriteTexture;
    int frameWidth;
    int frameHeight;
    int frameCount;
    int currentFrame;
    int totalFrames;
    SDL_Rect *frameRect;
public:
    SDL_Rect *spriteRect;
    void deathSound(int);
    bool isAlive = true; /** Parametar koji govori da li je sprite ziv (!Pogodjen od strane lopte) */
    bool isSkating; /** Da li Sprite trenutno kliza po ledu */
    void slide();
    bool hasCollision; /** Da li je sprite u koliziji sa nekim objektom. Sluzi za obustavljanje move()*/
    char state; /** State predstavlja stanje u kome se nalazi Sprite-levo,gore,desno,dole */
    bool isCollision(SpriteObject*,int,int); /** Provera kolizije sa objektima*/
    bool isCollision(Fireball* object); /** Provera kolizije sa vatrenim loptama*/
    Sprite(SDL_Renderer*);
    int lastDirection; /** Int koji mi je sluzio da znam koji je poslednji pravac kretanja Sprite-a bio */
    void draw(SDL_Renderer*);
    void drawDeath(SDL_Renderer*,TTF_Font*);
    void move(int, int);
    bool outOfBounds(int,int); /** Da li je sprite van margina mape */
};
#endif // SPRITE_H_INCLUDED
