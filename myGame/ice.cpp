#include "ice.h"

/** Konstruktor ledenih blokova */
Ice::Ice(SDL_Renderer *renderer,int xCoordinate, int yCoordinate,int fWidth,int fHeight,int fX,int fY) {
    isIce = true; /** Indikator za raspoznavanje objekta Ice/Block */
    frameWidth = 32;
    frameHeight = 32;

    SDL_Surface *surface = IMG_Load("resources/tilesets/88665.png"); /** Adresa sa koje se uzimaju koordinate leda */
    spriteTexture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);

    frameRect = new SDL_Rect;
    frameRect->x = fX; /** X koordinata leda sa slike */
    frameRect->y = fY; /** Y koordinata leda sa slike */
    frameRect->h = frameHeight; /** Visina slike */
    frameRect->w = frameWidth; /** Sirina slike */

    spriteRecta = new SDL_Rect;
    spriteRecta->x = xCoordinate;
    spriteRecta->y = yCoordinate;
    spriteRecta->w = fWidth; /** Sirina leda */
    spriteRecta->h = fHeight; /** Visina leda */
}

/** Metoda za crtanje leda */
void Ice::draw(SDL_Renderer *renderer) {
        SDL_RenderCopy(renderer, spriteTexture, frameRect, spriteRecta);
    }
