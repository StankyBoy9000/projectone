

#include <iostream>
#include <fstream> /** Zaglavlje biblioteke za rad sa file stream-ovima. */

#include "engine.h"
#include "level.h"

using namespace std;


int main(int argc, char** argv)
{
    Engine *eng = new Engine("RPG Game");
    eng->addTileset("resources/tilesets/snow_tileset.txt", "default");/** Dodavanje novog tileset-a. */
    ifstream levelStream("resources/levels/level1.txt");
    eng->addDrawable(new Level(levelStream, eng->getTileset("default"))); /** Dodavanje novog nivoa. */


    eng->addTileset("resources/tilesets/objects_tileset.txt", "default");  /** Dodavanje jos jednog tileseta, da bi mogla da bude vidljiva pozadina iza njih */
    ifstream objectStream("resources/levels/level1Objects.txt");
    eng->addDrawable(new Level(objectStream, eng->getTileset("default")));

    eng->run(); /** Pokretanje glavne petlje igre. */
    delete eng; /** Oslobadjanje memorije koju je zauzela instanca Engine klase. */
    return 0;
}
