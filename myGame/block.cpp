#include "block.h"

/** Konstruktor blokova */
/** Blokovi su svi objekti sa kojima Sprite ima koliziju osim: Zidova mape,ledenih blokova. */
Block::Block(SDL_Renderer *renderer,int xCoordinate, int yCoordinate,int fWidth,int fHeight,int fX,int fY) {
    frameWidth = fWidth;
    frameHeight = fHeight;

    SDL_Surface *surface = IMG_Load("resources/tilesets/88665.png");
    spriteTexture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);

    frameRect = new SDL_Rect;
    frameRect->x = fX;
    frameRect->y = fY;
    frameRect->h = frameHeight;
    frameRect->w = frameWidth;

    spriteRecta = new SDL_Rect;
    spriteRecta->x = xCoordinate;
    spriteRecta->y = yCoordinate;
    spriteRecta->w = fWidth;
    spriteRecta->h = fHeight;
}
/** Crtanje blokova */
void Block::draw(SDL_Renderer *renderer) {
    SDL_RenderCopy(renderer, spriteTexture, frameRect, spriteRecta);
}
