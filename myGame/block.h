#ifndef BLOCK_H_INCLUDED
#define BLOCK_H_INCLUDED
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "spriteObject.h"
#include <iostream>

using namespace std;

class Block : public SpriteObject{
public:
    Block(SDL_Renderer*,int, int,int,int,int,int);
    void draw(SDL_Renderer*);
};


#endif // BLOCK_H_INCLUDED
