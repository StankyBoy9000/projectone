#ifndef FIREBALL_H_INCLUDED
#define FIREBALL_H_INCLUDED
#include <SDL2/SDL.h>
#include "drawable.h"
class Fireball : public Drawable{
private:

    SDL_Texture *fireballTexture;
    int frameWidth;
    int frameHeight;
    SDL_Rect *frameRect;
public:
    SDL_Rect *fireballRect;
    bool active = true;
    int spriteX; /** X koordinata Sprite-a koga vatrena lopta gadja */
    int spriteY; /** Y koordinata Sprite-a koga vatrena lopta gadja */
    int fireballX;
    int fireballY;
    Fireball(SDL_Renderer*,int,int,int,int); /** Konstruktor uzima za parametre renderer i koordinate Sprite-a kao i trenutne koordinate misa */
    void draw(SDL_Renderer*);
    void move();
    bool outOfBounds();
};

#endif // FIREBALL_H_INCLUDED
