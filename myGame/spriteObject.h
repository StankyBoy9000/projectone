#ifndef SPRITEOBJECT_H_INCLUDED
#define SPRITEOBJECT_H_INCLUDED
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "drawable.h"

class SpriteObject : public Drawable{
protected:

    SDL_Texture *spriteTexture;
    int frameWidth;
    int frameHeight;
    int frameCount;
    int currentFrame;
    int totalFrames;
    SDL_Rect *frameRect;
public:
    SDL_Rect *spriteRecta; /** Isto sto i spriteRect, samo sam morao drugi naziv da mu dam. */
    char state;
    bool isIce = false; /** Da li je objekat led.(Da li se moze klizati po njemu) Po defaultu je false. */

    void draw(SDL_Renderer*);
};
#endif // SPRITE_H_INCLUDED
