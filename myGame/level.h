#ifndef LEVEL_H_INCLUDED
#define LEVEL_H_INCLUDED

#include <iostream>
#include <vector>

#include "drawable.h"
#include "tileset.h"

using namespace std;

class Level : public Drawable {
private:
    int x;
    int y;
    Tileset *tileset;
    vector<vector<char> > level;
public:
    Level(istream &inputStream, Tileset *tileset);
    virtual void draw(SDL_Renderer * renderer);
    void setPos(int x, int y) {
        this->x = x;
        this->y = y;
    }
    void move(int dx, int dy) {
        x += dx;
        y += dy;
    }
    ~Level();
};

#endif // LEVEL_H_INCLUDED
