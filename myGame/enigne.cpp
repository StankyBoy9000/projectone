#include "engine.h"
#include "sprite.h"
#include "spriteObject.h"
#include "block.h"
#include "ice.h"
#include "fireball.h"
#include <SDL2/SDL_ttf.h>

using namespace std;

Engine::Engine(string title) {
    SDL_Init(SDL_INIT_VIDEO);
    IMG_Init(IMG_INIT_PNG);
    window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_CENTERED, 640, 480, SDL_WINDOW_RESIZABLE);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
}

void Engine::addTileset(Tileset *tileset, const string &name) {
    tilesets[name] = tileset;
}

void Engine::addTileset(istream &inputStream, const string &name) {
    addTileset(new Tileset(inputStream, renderer), name);
}

void Engine::addTileset(const string &path, const string &name) {
    ifstream tilesetStream(path.c_str());
    addTileset(tilesetStream, name);
}

Tileset* Engine::getTileset(const string &name) {
    return tilesets[name];
}

void Engine::addDrawable(Drawable *drawable) {
    drawables.push_back(drawable);
}

Engine::~Engine() {
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void Engine::run() {
    int deathCounter = 0;
    SDL_Event event;
    TTF_Font* font = TTF_OpenFont("sans.ttf", 24);
    bool running = true; /** Da li engine radi */
    int startTime; /** Pocetno vreme celog ciklusa engina */
    int endTime; /** Krajnje vreme celog ciklusa engina */
    int maxDelay = 16; /** Broj frejmova na koji se ogranicava engine */
    /** StateX i StateY sluze da bi se odredio pravac kretanja Sprite-a, koristi se za koliziju. */
    int stateX = 0; /** Pocetno stanje X koordinate */
    int stateY = 0; /** Pocetno stanje Y koordinate */


    /** Kreiranje Sprite-a i svih objekata na mapi tipa Block i Ice */
    /** Block je neprohodan objekat */
    /** Ice je objekat kroz koji sprite moze da klizi */
    Sprite *sp = new Sprite(renderer);
    Block *tire = new Block(renderer,260,200,32,32,737,1951);
    Block *tower = new Block(renderer,500,-220,32*4,32*12,0,800);
    Ice *icePath = new Ice(renderer,300,50,32*2,32*2,32,224);
    Ice *icePath2 = new Ice(renderer,100,220,32*2,32*2,32,224);
    Ice *icePath3 = new Ice(renderer,350,200,16*2,32*2,32,224);
    Ice *icePath4 = new Ice(renderer,180,110,32*2,16*2,32,224);
    Ice *icePath5 = new Ice(renderer,420,200,16*2,16*2,32,224);
    Block *crate = new Block(renderer,550,200,64,64,384,448);
    Block *snowman = new Block(renderer,100,150,32,64,384,1056);
    Block *bush = new Block(renderer,32,32,64,64,416,96);

    /** Guranje svih objekata u jednu listu radi proveravanja kolizije Sprite-a i njih */
    objects.push_back(tire);
    objects.push_back(bush);
    objects.push_back(tower);
    objects.push_back(snowman);
    objects.push_back(crate);
    objects.push_back(icePath);
    objects.push_back(icePath2);
    objects.push_back(icePath3);
    objects.push_back(icePath4);
    objects.push_back(icePath5);

    /** Guranje svih objekata u drugu listu koja sluzi da se objekti crtaju */
    drawables.push_back(tire);
    drawables.push_back(bush);
    drawables.push_back(tower);
    drawables.push_back(snowman);
    drawables.push_back(crate);
    drawables.push_back(icePath);
    drawables.push_back(icePath2);
    drawables.push_back(icePath3);
    drawables.push_back(icePath4);
    drawables.push_back(icePath5);
    drawables.push_back(sp);

    while(running) {
        startTime = SDL_GetTicks();
        while(SDL_PollEvent(&event)) {
            switch(event.type) {
                case SDL_QUIT:
                    running = false;
                break;
                case SDL_MOUSEBUTTONDOWN: /** Na klik se registruju koordinate misa, i kreira se objekat Fireball na toj lokaciji tako sto se doda u listu fireballs koja se kasnije crta */
                    {
                    Fireball *fire = new Fireball(renderer,event.button.x,event.button.y,sp->spriteRect->x,sp->spriteRect->y);
                    fireballs.push_back(fire);
                    }
                    break;
                case SDL_KEYDOWN:
                    switch(event.key.keysym.sym) {
                    case SDLK_F5: /** Na pritisak F5 se Sprite ozivi */
                        sp->isAlive = true;
                        deathCounter = 0;
                        break;
                    case SDLK_LEFT:
                        sp->state |= 2;
                        break;
                    case SDLK_RIGHT:
                        sp->state |= 1;
                        break;
                    case SDLK_UP:
                        sp->state |= 4;
                        break;
                    case SDLK_DOWN:
                        sp->state |= 8;
                        break;
                    }
                    break;
                    case SDL_KEYUP:
                        switch(event.key.keysym.sym) {
                            case SDLK_LEFT:
                                sp->state &= 0b11111101;
                                break;
                            case SDLK_RIGHT:
                                sp->state &= 0b11111110;

                                break;
                            case SDLK_UP:
                                sp->state &= 0b11111011;

                                break;
                            case SDLK_DOWN:
                                sp->state &= 0b11110111;

                                break;
                        }
                        break;
            }
        }
        /** U slucaju da Sprite ne kliza proverava se na osnovu State-ova pravac kretanja i pamti se pravac u lastDirection */

        if(!sp->isSkating){
            if(sp->state & 1){
                stateX = 1;
                stateY = 0;
                sp->lastDirection = 4; /** Desno */
            }
            else if(sp->state & 2){
                stateX = -1;
                stateY = 0;
                sp->lastDirection = 1; /** Levo */
            }
            else if(sp->state & 4){
                stateX = 0;
                stateY = -1;
                sp->lastDirection = 2; /** Gore */
            }
            else if(sp->state & 8){
                stateX = 0;
                stateY = 1;
                sp->lastDirection = 3; /** Dole */
            }
            else if(sp->state & 0){ /** Stoji u mestu */
                stateX = 0;
                stateY = 0;
            }}


        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);
        /** isSkating i hasCollision su parametri koji odredjuju da li Sprite kliza ili je u koliziji sa nekim objektom */
        /** Podesavaju se posle svakog ciklusa na false da ne bi stojao u mesu ili beskonacno klizao nakon prvog kontakta sa objektom Block/Ice */
        sp->isSkating = false;
        sp->hasCollision = false;
        /** Prolazi se kroz sve objekte i proverava da li ima kolizije, u slucaju da ima proverava se da li je objekat led. */
        for(int i = 0; i < objects.size() ; i++){
            if(sp->isCollision(objects[i],stateX,stateY)){
               sp->hasCollision = true;
                if(objects[i]->isIce){
                        sp->isSkating = true;
                }
            }
        }
        /** Provera da li je doslo do kontakta vatrene lopte i Sprite-a, u slucaju da jeste, Sprite-u se menja vrednost isAlive u false(umire). */
        for(int i = 0; i < fireballs.size(); i++){
            if(sp->isCollision(fireballs[i])){
                sp->isAlive = false;
            }
        }
        /** Slide i move se uvek pozivaju, a u samim funkcijama se proverava da li ima potrebe za tim. U slucaju da nema, odmah se izlazi iz funkcije */
        sp->slide();
        sp->move(stateX, stateY);
        /** Nakon izvrsenog kretanja ili klizanja, State-ovi se vracaju na default vrednost. */
        stateX = 0;
        stateY = 0;
        /** Crtanje svih objekata. */
        for(size_t i = 0; i < drawables.size(); i++) {
            drawables[i]->draw(renderer);
        }
        /** Crtanje svih aktivnih vatrenih loptica */
        for(int i = int(fireballs.size()) -1; i >= 0; i--){
            if(!fireballs[i]->active){
                fireballs.erase(fireballs.begin()+i);
            }
            /** Odmah nakos to se nacrta loptica poziva se i move, jer ako postoji onda mora i da se krece. */
            fireballs[i]->draw(renderer);
            fireballs[i]->move();
        }
        /** U slucaju da je Sprite ziv crta se, u suprotnom ne. */
        if(sp->isAlive){
            sp->draw(renderer);
        }
        if(!sp->isAlive){
            sp->deathSound(deathCounter);
            deathCounter++;
        }
        /** U slucaju da je Sprite mrtav, crta se tekst. */
        sp->drawDeath(renderer,font);

        SDL_RenderPresent(renderer);
        endTime = SDL_GetTicks();
        /** Odredjuje se vreme celog ciklusa engina, u slucaju da je prebrzo prosao pozviva se sdl_delay. */
        if(endTime - startTime < maxDelay) {
            SDL_Delay(maxDelay - (endTime - startTime));
        }
    }
}
